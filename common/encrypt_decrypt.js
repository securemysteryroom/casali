const config = require('./../config.json');

function encodeDecode(s) {
    // s=JSON.stringify(s);
    var enc = "";
    var str = "";
    // make sure that input is string
    str = s.toString();
    for (var i = 0; i < s.length; i++) {
        // create block
        var a = s.charCodeAt(i);
        // bitwise XOR
        var b = a ^ config.ENCRYPT_DECRYPT_KEY;
        enc = enc + String.fromCharCode(b);
    }
    return enc;
}

module.exports = { encodeDecode };
