const config = require('./config.json');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
// const multipart = require('connect-multiparty');
// app.use(multipart());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use("/uploads", express.static('uploads'));			// images folder set public

app.use('/admin/auth', require('./routes/admin/auth'));
app.use('/admin/property', require('./routes/admin/property'));
app.use('/admin/apartment', require('./routes/admin/apartment'));
app.use('/admin/tenant', require('./routes/admin/tenant'));
app.use('/admin/collector', require('./routes/admin/collector'));

app.use('/collector/auth', require('./routes/collector/auth'));
app.use('/collector/payment', require('./routes/collector/payment'));
app.use('/collector/list', require('./routes/collector/list'));
app.use('/collector/bank', require('./routes/collector/bank'));
app.use('/collector/recipient', require('./routes/collector/recipient'));

app.use('/tenant/auth', require('./routes/tenant/auth'));

const mongod = require('mongodb');
const MongoClient = mongod.MongoClient;
ObjectID = module.exports = mongod.ObjectID;
// MongoClient.connect('mongodb://'+config.DB_HOST+':'+config.DB_PORT+'/'+config.DB_NAME,
MongoClient.connect('mongodb://'+config.DB_USERNAME+':'+config.DB_PASSWORD+'@'+config.DB_HOST+':'+config.DB_PORT+'/'+config.DB_NAME,
function(err, dclient){
    if(err) {
        console.log("Mongodb connection in error >> ", err);
    }else{
        console.log("Mongodb connected.");
        db = module.exports = dclient.db(config.DB_NAME);
    }
});

app.listen(config.PORT);
