const jwtAuth = require('../../common/jwt_auth');
const express = require('express');
const router = express.Router();

router.post('/', jwtAuth.ensureToken, submit);
router.get('/', jwtAuth.ensureToken, listPaymentHistory);

async function submit(req, res){		// tenantId, totalAmount, paidAmount, type
    if(!req.body.uid || req.body.uid.length != 24 || !req.body.tenantId || req.body.tenantId.length != 24 || !req.body.totalAmount || !req.body.paidAmount || !req.body.type)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('collector').findOne({ _id: ObjectID(req.body.uid) },function(err, collectorInfo){
		if(collectorInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
            
        let insertObj = {
            collectorId: ObjectID(req.body.uid),
            tenantId: ObjectID(req.body.tenantId),
            totalAmount: req.body.totalAmount,
            paidAmount: req.body.paidAmount,
            type: req.body.type,
            cd: new Date()
        }
        db.collection('payment_details').insertOne(insertObj, function(err, paymentData) {
            if(paymentData && paymentData.ops && paymentData.ops[0])
                return res.send({message: "Payment details added successfully.", status: 1, data: paymentData.ops[0]})

            return res.send({message: "Something went wrong.", status: 0, data: {}})
        })
	})
}

async function listPaymentHistory(req, res){        // skip
    if(!req.body.uid || req.body.uid.length != 24 || !req.query.skip)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('collector').findOne({ _id: ObjectID(req.body.uid) },function(err, collectorInfo){
		if(collectorInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
        
        db.collection('payment_details').find({collectorId: ObjectID(req.body.uid)}).skip(parseInt(req.query.skip)).limit(10).sort({cd: -1}).toArray(function(err, paymentHistoryList){
            res.send({message: "Payment history get successfully.", status: 1, data: {paymentHistoryList}})
        })
	})
}

module.exports = router;