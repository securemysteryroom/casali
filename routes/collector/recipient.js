const jwtAuth = require('../../common/jwt_auth');
const express = require('express');
const router = express.Router();
const multer = require('multer');

const Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./uploads/bankReceiptImages");
    },
    filename: function(req, file, callback) {
		const filename = file.fieldname + '_' + Date.now() + '_' + Math.floor(1000 + Math.random() * 9000) + '.png';
        callback(null, filename);
    }
});
const upload = multer({
    storage: Storage
})

router.post('/', jwtAuth.ensureToken, addRecipient);
router.get('/', jwtAuth.ensureToken, getRecipient);
router.get('/categories', jwtAuth.ensureToken, getCategories);
router.post('/payment', jwtAuth.ensureToken, upload.fields([{ name: 'signature', maxCount: 1 }, { name: 'receipt', maxCount: 1 }]), submitPayment);
router.get('/paymentHistory', jwtAuth.ensureToken, listPaymentHistory);

async function getCategories(req, res){
    if(!req.body.uid || req.body.uid.length != 24)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

    db.collection('recipient_categories').find({ }).toArray(function(err, recipientCategoriesList){
        res.send({message: "Recipient categories get successfully.", status: 1, data: {recipientCategoriesList}})
    })
}

async function getRecipient(req, res){
    if(!req.body.uid || req.body.uid.length != 24)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

    db.collection('recipients').find({collectorId: ObjectID(req.body.uid)}).toArray(function(err, recipientsList){
        res.send({message: "Recipients list get successfully.", status: 1, data: {recipientsList}})
    })
}

async function addRecipient(req, res){		// recipientName, categoryId
    if(!req.body.uid || req.body.uid.length != 24 || !req.body.recipientName || req.body.recipientName.length == 0 || !req.body.categoryId || req.body.categoryId.length != 24)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('collector').findOne({ _id: ObjectID(req.body.uid) },function(err, collectorInfo){
		if(collectorInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
            
        let insertObj = {
            collectorId: ObjectID(req.body.uid),
            categoryId: ObjectID(req.body.categoryId),
            recipientName: req.body.recipientName,
            cd: new Date()
        }
        db.collection('recipients').insertOne(insertObj, function(err, recipientData) {
            if(recipientData && recipientData.ops && recipientData.ops[0])
                return res.send({message: "Recipient details added successfully.", status: 1, data: recipientData.ops[0]})

            return res.send({message: "Something went wrong.", status: 0, data: {}})
        })
	})
}

async function submitPayment(req, res){		// recipientId, amount
    if(!req.body.uid || req.body.uid.length != 24 || !req.body.recipientId || req.body.recipientId.length != 24 || !req.body.amount)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('collector').findOne({ _id: ObjectID(req.body.uid) },function(err, collectorInfo){
		if(collectorInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
            
        let insertObj = {
            collectorId: ObjectID(req.body.uid),
            recipientId: ObjectID(req.body.recipientId),
            amount: req.body.amount,
            cd: new Date()
        }
        if(typeof req.files.signature != "undefined" && typeof req.files.receipt != "undefined"){
            insertObj.signature = req.files.signature[0].path.replace(/\\/g, "/");
            insertObj.receipt = req.files.receipt[0].path.replace(/\\/g, "/");
        }else if(typeof req.files.signature != "undefined"){
            insertObj.signature = req.files.signature[0].path.replace(/\\/g, "/");
        }else if(typeof req.files.receipt != "undefined"){
            insertObj.receipt = req.files.receipt[0].path.replace(/\\/g, "/");
        }
        db.collection('recipient_payment_details').insertOne(insertObj, function(err, paymentData) {
            if(paymentData && paymentData.ops && paymentData.ops[0])
                return res.send({message: "Recipient payment details added successfully.", status: 1, data: paymentData.ops[0]})

            return res.send({message: "Something went wrong.", status: 0, data: {}})
        })
	})
}

async function listPaymentHistory(req, res){        // skip
    if(!req.body.uid || req.body.uid.length != 24 || !req.query.skip)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('collector').findOne({ _id: ObjectID(req.body.uid) },function(err, collectorInfo){
		if(collectorInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
        
        db.collection('recipient_payment_details').aggregate([
            {$match:{collectorId: ObjectID(req.body.uid)}},
            {$lookup:{from: "recipients", localField: "recipientId", foreignField: "_id", as:"recipient_details"}},
            { $sort : { cd : -1 } },
            { $skip: parseInt(req.query.skip) },
            { $limit: 10 }
            ]).toArray(function(err, paymentHistoryList){
            res.send({message: "Recipient payment history get successfully.", status: 1, data: {paymentHistoryList}})
        })
	})
}

module.exports = router;