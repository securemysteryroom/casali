const jwtAuth = require('../../common/jwt_auth');
const express = require('express');
const router = express.Router();
const multer = require('multer');

const Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./uploads/bankReceiptImages");
    },
    filename: function(req, file, callback) {
		const filename = file.fieldname + '_' + Date.now() + '_' + Math.floor(1000 + Math.random() * 9000) + '.png';
        callback(null, filename);
    }
});
const upload = multer({
    storage: Storage
})

router.post('/', jwtAuth.ensureToken, upload.single('photo'), submit);
router.get('/', jwtAuth.ensureToken, listBankDetails);
router.get('/depositeHistory', jwtAuth.ensureToken, depositeHistory);

async function submit(req, res){		// bankId, amount
    if(!req.body.uid || req.body.uid.length != 24 || !req.body.bankId || req.body.bankId.length != 24 || !req.body.amount)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('collector').findOne({ _id: ObjectID(req.body.uid) },function(err, collectorInfo){
		if(collectorInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
            
        var photo = "";
        if(req.file && req.file.path){
            photo = req.file.path.replace(/\\/g, "/");
        }
        let insertObj = {
            collectorId: ObjectID(req.body.uid),
            bankId: ObjectID(req.body.bankId),
            amount: req.body.amount,
            photo,
            cd: new Date()
        }
        db.collection('bank_payment_details').insertOne(insertObj, function(err, paymentData) {
            if(paymentData && paymentData.ops && paymentData.ops[0])
                return res.send({message: "Bank payment details added successfully.", status: 1, data: paymentData.ops[0]})

            return res.send({message: "Something went wrong.", status: 0, data: {}})
        })
	})
}

async function listBankDetails(req, res){
    if(!req.body.uid || req.body.uid.length != 24)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('collector').findOne({ _id: ObjectID(req.body.uid) },function(err, collectorInfo){
		if(collectorInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
        
        db.collection('bank_details').find({active: true}).toArray(function(err, bankDetailsList){
            res.send({message: "Bank details get successfully.", status: 1, data: {bankDetailsList}})
        })
	})
}

async function depositeHistory(req, res){        // skip
    if(!req.body.uid || req.body.uid.length != 24 || !req.query.skip)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('collector').findOne({ _id: ObjectID(req.body.uid) },function(err, collectorInfo){
		if(collectorInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
        
        db.collection('bank_payment_details').find({collectorId: ObjectID(req.body.uid)}).skip(parseInt(req.query.skip)).limit(10).sort({cd: -1}).toArray(function(err, paymentHistoryList){
            res.send({message: "Bank payment details get successfully.", status: 1, data: {paymentHistoryList}})
        })
	})
}

module.exports = router;
