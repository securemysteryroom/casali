const jwtAuth = require('../../common/jwt_auth');
const encryptDecrypt = require('../../common/encrypt_decrypt');
const express = require('express');
const router = express.Router();

router.get('/', jwtAuth.ensureToken, listPropertyTenant);
router.get('/apartment', jwtAuth.ensureToken, getApartment);
router.get('/tenant', jwtAuth.ensureToken, getTenant);

async function listPropertyTenant(req, res){
	if(!req.body.uid || req.body.uid.length != 24)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('collector').aggregate([
        {$match:{"_id" : ObjectID(req.body.uid)}},
        {$lookup:{from:"property", localField:"property", foreignField: "_id", as: "property"}},
        {$project:{"property._id":1, "property.name":1}}
    ]).toArray(function(err, collectorPropertyInfo){
		if(collectorPropertyInfo.length == 0)
			return res.send({message: "User not found.", status: 0, data: {}})
        
        if(collectorPropertyInfo[0].property.length == 0)
            return res.send({message: "Property not found.", status: 0, data: {}})

        let propertyIds = [];
        for(let i = 0; i < collectorPropertyInfo[0].property.length; i++){
            collectorPropertyInfo[0].property[i].tenant = [];
            propertyIds.push(collectorPropertyInfo[0].property[i]._id);
        }
		db.collection('tenant').find({property: {$in: propertyIds}}).project({_id:1, property:1, name:1, deposit:1, photo:1, apartment:1}).sort({created_date: -1}).toArray(function(err, tenantList){
            let ids = [];
            propertyIds.map(id => { return ids.push(id.toString()); });
            let finalResponse = collectorPropertyInfo[0].property;
            tenantList.forEach(tenant => {
                let index = ids.indexOf(tenant.property.toString());
                finalResponse[index].tenant.push(tenant);
            });
			res.send({message: "Property Tenant get successfully.", status: 1, data: {property: finalResponse}})
		})
	})
}

async function getApartment(req, res){
	if(!req.body.uid || req.body.uid.length != 24 || !req.query.apartmentId || req.query.apartmentId.length != 24)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

    db.collection('apartment').findOne({ _id: ObjectID(req.query.apartmentId) },function(err, apartmentInfo){
        if(apartmentInfo == null)
            return res.send({message: "User not found.", status: 0, data: {}})
        
        apartmentInfo.monthly_payments.push({_id: apartmentInfo._id, name: "Rent", value: apartmentInfo.rent});
        for(let i = 0; i < apartmentInfo.monthly_payments.length; i++){
            apartmentInfo.monthly_payments[i].osPerMonth = 0;
            apartmentInfo.monthly_payments[i].totalOs = 0;
            apartmentInfo.monthly_payments[i].monthlyRent = apartmentInfo.monthly_payments[i].value;
            delete apartmentInfo.monthly_payments[i].value;
        }
        return res.send({message: "Apartment info get successfully.", status: 1, data: apartmentInfo})
    })
}

async function getTenant(req, res){
	if(!req.query.tenantId || req.query.tenantId.length != 24)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('tenant').findOne({ _id: ObjectID(req.query.tenantId) },function(err, tenantInfo){
		if(tenantInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
			
		return res.send({message: "Tenant info get successfully.", status: 1, data: tenantInfo})
	})
}

module.exports = router;
