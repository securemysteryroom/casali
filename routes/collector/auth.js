const config = require('../../config.json');
const jwtAuth = require('../../common/jwt_auth');
const encryptDecrypt = require('../../common/encrypt_decrypt');
const jwt = require('jsonwebtoken');
const express = require('express');
const router = express.Router();
const multer = require('multer');

const Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./uploads/collectorImages");
    },
    filename: function(req, file, callback) {
		const filename = file.fieldname + '_' + Date.now() + '_' + Math.floor(1000 + Math.random() * 9000) + '.png';
        callback(null, filename);
    }
});
const upload = multer({
    storage: Storage
})

router.post('/login_collector', loginCollector);
router.post('/change_password_collector', jwtAuth.ensureToken, changePasswordCollector);
router.get('/', jwtAuth.ensureToken, collectorProfileInfo);
router.put('/', jwtAuth.ensureToken, upload.single('photo'), collectorProfileUpdate);

async function loginCollector(req, res){			// email, password
	if(!req.body.email || !req.body.password)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('collector').findOne({ email: req.body.email },function(err, collectorInfo){
		if(collectorInfo == null)
			return res.send({message: "Email not registered.", status: 0, data: {}})

		let hash = encryptDecrypt.encodeDecode(req.body.password);
		if(collectorInfo.password != hash)
			return res.send({message: "You have entered wrong password.", status: 0, data: {}})
			
		const token = jwt.sign({ uid: collectorInfo._id }, config.JWT_SECRET_KEY);       					// token expire default time is 7 days
		// const token = jwt.sign({ uid: collectorInfo.uid }, config.JWT_SECRET_KEY,{ expiresIn: 20 });		// token expire in 20 seconds
		// const token = jwt.sign({ uid: collectorInfo.uid }, config.JWT_SECRET_KEY,{ expiresIn: "20" });   // token expire in 20 miliseconds, you can also set token expire time "2 days", "10h", "7d"
		collectorInfo.token = token;
		collectorInfo.uid = collectorInfo._id;
		collectorInfo.image_url = config.IMAGE_URL;
		delete collectorInfo._id
		delete collectorInfo.password;
		return res.send({message: "Collector login successfully.", status: 1, data: collectorInfo})
	})
}

async function changePasswordCollector(req, res){		// oldPassword, newPassword
	if(!req.body.uid || req.body.uid.length != 24 || !req.body.oldPassword || !req.body.newPassword)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('collector').findOne({ _id: ObjectID(req.body.uid) },function(err, collectorInfo){
		if(collectorInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
			
		let hash = encryptDecrypt.encodeDecode(req.body.oldPassword);
		if(collectorInfo.password != hash)
			return res.send({message: "You have entered wrong old password.", status: 0, data: {}})

		let newHash = encryptDecrypt.encodeDecode(req.body.newPassword);
		db.collection("collector").updateOne({ _id: ObjectID(req.body.uid) }, { $set: { password: newHash }}, function(){ });
		return res.send({message: "Password update successfully.", status: 1, data: {}})
	})
}

async function collectorProfileInfo(req, res){
	if(!req.body.uid || req.body.uid.length != 24)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('collector').aggregate([
        {$match:{"_id" : ObjectID(req.body.uid)}},
        {$lookup:{from:"property", localField:"property", foreignField: "_id", as: "property"}},
        {$project:{"property._id":1, "property.name":1, "property.multimedia":1,"first_name":1,"last_name":1,"deposit":1,"trn":1,"telephone":1,"facebook":1,"whatsapp":1,"photo":1}}
    ]).toArray(function(err, collectorInfo){
		if(collectorInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
			
		return res.send({message: "Collector info get successfully.", status: 1, data: collectorInfo})
	})
}

async function collectorProfileUpdate(req, res){
	let updateObj = {};
	if(req.file && req.file.path){
		updateObj.photo = req.file.path.replace(/\\/g, "/");
	}
	if(req.body.first_name){
		updateObj.first_name = req.body.first_name;
	}
	if(req.body.last_name){
		updateObj.last_name = req.body.last_name;
	}
	if(req.body.trn){
		updateObj.trn = req.body.trn;
	}
	// if(req.body.property){
	// 	updateObj.property = [];
	// 	req.body.property.forEach(element => {
	// 		updateObj.property.push(ObjectID(element));
	// 	});
	// }
	if(req.body.telephone){
		updateObj.telephone = req.body.telephone;
	}
	if(req.body.facebook){
		updateObj.facebook = req.body.facebook;
	}
	if(req.body.whatsapp){
		updateObj.whatsapp = req.body.whatsapp;
	}
	if(Object.keys(updateObj).length === 0)
		return res.send({message: "No data for update.", status: 0, data: {}})

	db.collection("collector").updateOne({ _id: ObjectID(req.body.uid) }, { $set: updateObj}, function(){ });
	return res.send({message: "Collector details update successfully.", status: 1, data: {}})
}

module.exports = router;
