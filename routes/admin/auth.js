const config = require('../../config.json');
const jwtAuth = require('../../common/jwt_auth');
const encryptDecrypt = require('../../common/encrypt_decrypt');
const jwt = require('jsonwebtoken');
const express = require('express');
const router = express.Router();

// router.post('/register_admin', registerAdmin);
router.post('/login_admin', loginAdmin);
// router.post('/change_password_admin', jwtAuth.ensureToken, changePasswordAdmin);

async function registerAdmin(req, res){		// email, password
    if(!req.body.email || !req.body.password)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('admin').findOne({ email: req.body.email },function(err, adminInfo){
		if(adminInfo != null)
			return res.send({message: "Email already exists.", status: 0, data: {}})

		let hash = encryptDecrypt.encodeDecode(req.body.password);
		db.collection('admin').insertOne({ email: req.body.email, password: hash }, function(err, adminData) {
			if(adminData && adminData.ops && adminData.ops[0]){
				delete adminData.ops[0].password;
				return res.send({message: "Admin registered successfully.", status: 1, data: adminData.ops[0]})
			}
			
			return res.send({message: "Something went wrong.", status: 0, data: {}})
		})
	})
}

async function loginAdmin(req, res){			// email, password
	if(!req.body.email || !req.body.password)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('admin').findOne({ email: req.body.email },function(err, adminInfo){
		if(adminInfo == null)
			return res.send({message: "Email not registered.", status: 0, data: {}})

		let hash = encryptDecrypt.encodeDecode(req.body.password);
		if(adminInfo.password != hash)
			return res.send({message: "You have entered wrong password.", status: 0, data: {}})
			
		const token = jwt.sign({ uid: adminInfo._id }, config.JWT_SECRET_KEY);       					// token expire default time is 7 days
		// const token = jwt.sign({ uid: adminInfo.uid }, config.JWT_SECRET_KEY,{ expiresIn: 20 });		// token expire in 20 seconds
		// const token = jwt.sign({ uid: adminInfo.uid }, config.JWT_SECRET_KEY,{ expiresIn: "20" });   // token expire in 20 miliseconds, you can also set token expire time "2 days", "10h", "7d"
		adminInfo.token = token;
		adminInfo.uid = adminInfo._id;
		adminInfo.image_url = config.IMAGE_URL;
		delete adminInfo._id
		delete adminInfo.password;
		return res.send({message: "Admin login successfully.", status: 1, data: adminInfo})
	})
}

async function changePasswordAdmin(req, res){		// oldPassword, newPassword
	if(!req.body.uid || req.body.uid.length != 24 || !req.body.oldPassword || !req.body.newPassword)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('admin').findOne({ _id: ObjectID(req.body.uid) },function(err, adminInfo){
		if(adminInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
			
		let hash = encryptDecrypt.encodeDecode(req.body.oldPassword);
		if(adminInfo.password != hash)
			return res.send({message: "You have entered wrong old password.", status: 0, data: {}})

		let newHash = encryptDecrypt.encodeDecode(req.body.newPassword);
		db.collection("admin").updateOne({ _id: ObjectID(req.body.uid) }, { $set: { password: newHash }}, function(){ });
		return res.send({message: "Password update successfully.", status: 1, data: {}})
	})
}

module.exports = router;
