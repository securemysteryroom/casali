const jwtAuth = require('../../common/jwt_auth');
const encryptDecrypt = require('../../common/encrypt_decrypt');
const express = require('express');
const router = express.Router();
const multer = require('multer');

const Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./uploads/tenantImages");
    },
    filename: function(req, file, callback) {
		const filename = file.fieldname + '_' + Date.now() + '_' + Math.floor(1000 + Math.random() * 9000) + '.png';
        callback(null, filename);
    }
});
const upload = multer({
    storage: Storage
})

router.post('/', jwtAuth.ensureToken, upload.single('photo'), addTenant);
router.get('/', jwtAuth.ensureToken, listTenant);

async function addTenant(req, res){		// property, apartment, name, deposit, telephone, trn, email, facebook, whatsapp
    if(!req.body.uid || req.body.uid.length != 24 || !req.body.property || !req.body.apartment || !req.body.name || !req.body.deposit || !req.body.telephone || !req.body.trn || !req.body.email || !req.body.facebook || !req.body.whatsapp)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('admin').findOne({ _id: ObjectID(req.body.uid) },function(err, adminInfo){
		if(adminInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})

		if(req.body.telephone.length === 0)		// we need at least one telephone number for generate tenant password
			return res.send({message: "Please add at least one telephone number.", status: 0, data: {}})

		db.collection('tenant').findOne({ email: req.body.email },async function(err, tenantInfo){
			if(tenantInfo != null)
				return res.send({message: "Email already exists.", status: 0, data: {}})

			var photo = "";
			if(req.file && req.file.path){
				photo = req.file.path.replace(/\\/g, "/");
			}
			let insertObj = {
				uid: ObjectID(req.body.uid),
				property: ObjectID(req.body.property),
				apartment: ObjectID(req.body.apartment),
				name: req.body.name,
				deposit: parseInt(req.body.deposit),
				telephone: req.body.telephone,
				trn: parseInt(req.body.trn),
				email: req.body.email,
				password: encryptDecrypt.encodeDecode(req.body.telephone[0]),
				facebook: req.body.facebook,
				whatsapp: req.body.whatsapp,
				photo,
				created_date: new Date()
			}
			db.collection('tenant').insertOne(insertObj, function(err, tenantData) {
				if(tenantData && tenantData.ops && tenantData.ops[0])
					return res.send({message: "Tenant added successfully.", status: 1, data: tenantData.ops[0]})
	
				return res.send({message: "Something went wrong.", status: 0, data: {}})
			})
		})
	})
}

async function listTenant(req, res){		// skip
	if(!req.body.uid || req.body.uid.length != 24 || !req.query.skip)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('admin').findOne({ _id: ObjectID(req.body.uid) }, function(err, adminInfo){
		if(adminInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
			
		db.collection('tenant').find({ }).skip(parseInt(req.query.skip)).limit(10).sort({created_date: -1}).toArray(function(err, tenantList){
			res.send({message: "Tenant get successfully.", status: 1, data: {tenantList}})
		})
	})
}

module.exports = router;
