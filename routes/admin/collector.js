const jwtAuth = require('../../common/jwt_auth');
const encryptDecrypt = require('../../common/encrypt_decrypt');
const express = require('express');
const router = express.Router();
const multer = require('multer');

const Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./uploads/collectorImages");
    },
    filename: function(req, file, callback) {
		const filename = file.fieldname + '_' + Date.now() + '_' + Math.floor(1000 + Math.random() * 9000) + '.png';
        callback(null, filename);
    }
});
const upload = multer({
    storage: Storage
})

router.post('/', jwtAuth.ensureToken, upload.single('photo'), addCollector);
router.get('/', jwtAuth.ensureToken, listCollector);

async function addCollector(req, res){		// first_name, last_name, deposit, trn, property, telephone, email, facebook, whatsapp
    if(!req.body.uid || req.body.uid.length != 24 || !req.body.first_name || !req.body.last_name || !req.body.deposit || !req.body.trn || !req.body.property || !req.body.telephone || !req.body.email |!req.body.facebook || !req.body.whatsapp)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('admin').findOne({ _id: ObjectID(req.body.uid) },function(err, adminInfo){
		if(adminInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})

		if(req.body.telephone.length === 0)		// we need at least one telephone number for generate collector password
			return res.send({message: "Please add at least one telephone number.", status: 0, data: {}})

		db.collection('collector').findOne({ email: req.body.email },async function(err, collectorInfo){
			if(collectorInfo != null)
				return res.send({message: "Email already exists.", status: 0, data: {}})

			var photo = "";
			if(req.file && req.file.path){
				photo = req.file.path.replace(/\\/g, "/");
			}
			const property = [];
			if(req.body.property && req.body.property.length > 0){
				req.body.property.forEach(element => {
					property.push(ObjectID(element));
				});
			}
			let insertObj = {
				uid: ObjectID(req.body.uid),
				first_name: req.body.first_name,
				last_name: req.body.last_name,
				deposit: parseInt(req.body.deposit),
				trn: parseInt(req.body.trn),
				property,
				telephone: req.body.telephone,
				email: req.body.email,
				password: encryptDecrypt.encodeDecode(req.body.telephone[0]),
				facebook: req.body.facebook,
				whatsapp: req.body.whatsapp,
				photo,
				created_date: new Date()
			}
			db.collection('collector').insertOne(insertObj, function(err, collectorData) {
				if(collectorData && collectorData.ops && collectorData.ops[0])
					return res.send({message: "Collector added successfully.", status: 1, data: collectorData.ops[0]})
	
				return res.send({message: "Something went wrong.", status: 0, data: {}})
			})
		})
	})
}

async function listCollector(req, res){		// skip
	if(!req.body.uid || req.body.uid.length != 24 || !req.query.skip)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('admin').findOne({ _id: ObjectID(req.body.uid) }, function(err, adminInfo){
		if(adminInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
			
		db.collection('collector').find({ }).skip(parseInt(req.query.skip)).limit(10).sort({created_date: -1}).toArray(function(err, collectorList){
			res.send({message: "Collector get successfully.", status: 1, data: {collectorList}})
		})
	})
}

module.exports = router;
