const jwtAuth = require('../../common/jwt_auth');
const express = require('express');
const router = express.Router();
const multer = require('multer');

const Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./uploads/propertyImages");
    },
    filename: function(req, file, callback) {
		const filename = file.fieldname + '_' + Date.now() + '_' + Math.floor(1000 + Math.random() * 9000) + '.png';
        callback(null, filename);
    }
});
const upload = multer({
    storage: Storage
})

const uploadFields = upload.fields([{ name: 'multimedia', maxCount: 10 }, { name: 'background_image', maxCount: 1 }]);
router.post('/', jwtAuth.ensureToken, uploadFields, addProperty);
router.get('/', jwtAuth.ensureToken, listProperty);
router.get('/type', jwtAuth.ensureToken, listPropertyType);
// router.put('/', jwtAuth.ensureToken, updateTodo);
// router.delete('/', jwtAuth.ensureToken, deleteTodo);

async function addProperty(req, res){		// name, address, type, floors, units, tax_value, tax_folio, tax_id, annual_property_taxes, collector, multimedia, background_image, location
	if(!req.body.uid || req.body.uid.length != 24 || !req.body.name || !req.body.address || !req.body.type || !req.body.floors || !req.body.units || !req.body.tax_value || !req.body.tax_folio || !req.body.tax_id || !req.body.annual_property_taxes || !req.body.location)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('admin').findOne({ _id: ObjectID(req.body.uid) },function(err, adminInfo){
		if(adminInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})

		const multimedia = [];
		if(req.files.multimedia.length > 0){
			req.files.multimedia.forEach(element => {
				multimedia.push(element.path.replace(/\\/g, "/"));
			});
		}
		let background_image = "";
		if(req.files.background_image.length > 0)
			background_image = req.files.background_image[0].path.replace(/\\/g, "/");
		
		if(req.body.collector && req.body.collector.length > 0)
			req.body.collector = ObjectID(req.body.collector);
		else
			req.body.collector = "";

		let insertObj = {
			uid: ObjectID(req.body.uid),
			name: req.body.name,
			address: req.body.address,
			type: req.body.type,
			floors: parseInt(req.body.floors),
			units: parseInt(req.body.units),
			tax_value: parseInt(req.body.tax_value),
			tax_folio: parseInt(req.body.tax_folio),
			tax_id: parseInt(req.body.tax_id),
			annual_property_taxes: parseInt(req.body.annual_property_taxes),
			collector: req.body.collector,
			location: req.body.location,
			multimedia,
			background_image,
			created_date: new Date()
		}
		db.collection('property').insertOne(insertObj, function(err, propertyData) {
			if(propertyData && propertyData.ops && propertyData.ops[0])
				return res.send({message: "Property added successfully.", status: 1, data: propertyData.ops[0]})

			return res.send({message: "Something went wrong.", status: 0, data: {}})
		})
	})
}

async function listProperty(req, res){		// skip
	if(!req.body.uid || req.body.uid.length != 24 || !req.query.skip)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('admin').findOne({ _id: ObjectID(req.body.uid) }, function(err, adminInfo){
		if(adminInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
			
		db.collection('property').find({ }).skip(parseInt(req.query.skip)).limit(10).sort({created_date: -1}).toArray(function(err, propertyList){
			res.send({message: "Property get successfully.", status: 1, data: {propertyList}})
		})
	})
}

async function listPropertyType(req, res){
	if(!req.body.uid || req.body.uid.length != 24)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('admin').findOne({ _id: ObjectID(req.body.uid) }, function(err, adminInfo){
		if(adminInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
			
		db.collection('property_type').find({ }).toArray(function(err, propertyTypeList){
			res.send({message: "Property type get successfully.", status: 1, data: {propertyTypeList}})
		})
	})
}

// async function updateTodo(req, res){		// todo_id, date/title/status
// 	if(req.body.todo_id){
// 		let parameters = "id = ?";
// 		let values = [req.body.todo_id];
// 		if(req.body.date){
// 			parameters = parameters+", date = ?";
// 			values.push(req.body.date);
// 		}
// 		if(req.body.title){
// 			parameters = parameters+", title = ?";
// 			values.push(req.body.title);
// 		}
// 		if(req.body.status){
// 			parameters = parameters+", status = ?";
// 			values.push(req.body.status);
// 		}
// 		if(values.length == 1){
// 			res.send({message: "Invalid parameters.", status: 0, data: {}})
// 			return false;
// 		}
// 		values.push(req.body.todo_id).push(req.body.id);
// 		let query = "update todo set "+parameters+" where id = ? and user_id = ?";
//     	let resp = await queryService.fireQuery(query,values);
//     	if(resp && resp.changedRows){
// 			res.send({message: "Todo updated successfully.", status: 1, data: {todo_id: req.body.todo_id}})
//     	}else{
//     		res.send({message: "Something went wrong.", status: 0, data: {}})
//     	}
// 	}else{
// 		res.send({message: "Invalid parameters.", status: 0, data: {}})
// 	}
// }

// async function deleteTodo(req, res){		// todo_id
// 	if(req.body.todo_id){
// 		let query = "delete from todo where id = ? and user_id = ?";
//     	let resp = await queryService.fireQuery(query,[req.body.todo_id, req.body.id]);
//     	if(resp && resp.changedRows){
// 			res.send({message: "Todo deleted successfully.", status: 1, data: {todo_id: req.body.todo_id}})
//     	}else{
//     		res.send({message: "Something went wrong.", status: 0, data: {}})
//     	}
// 	}else{
// 		res.send({message: "Invalid parameters.", status: 0, data: {}})
// 	}
// }

module.exports = router;
