const jwtAuth = require('../../common/jwt_auth');
const express = require('express');
const router = express.Router();
const multer = require('multer');

const Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./uploads/apartmentImages");
    },
    filename: function(req, file, callback) {
		const filename = file.fieldname + '_' + Date.now() + '_' + Math.floor(1000 + Math.random() * 9000) + '.png';
        callback(null, filename);
    }
});
const upload = multer({
    storage: Storage
})

router.post('/', jwtAuth.ensureToken, upload.array('multimedia', 10), addApartment);
router.get('/', jwtAuth.ensureToken, listApartment);
router.get('/monthly-payments', jwtAuth.ensureToken, listMonthlyPayments);

async function addApartment(req, res){		// propertyId, simple_unit, name, size, rent, deposit, monthly_payments, tenant, bedrooms, bathrooms, balcony, backyard, lawn, utilities, multimedia
    if(!req.body.uid || req.body.uid.length != 24 || !req.body.propertyId || !req.body.simple_unit || !req.body.name || !req.body.size || !req.body.rent || !req.body.deposit || !req.body.monthly_payments || !req.body.tenant || !req.body.bedrooms || !req.body.bathrooms || !req.body.balcony || !req.body.backyard || !req.body.lawn || !req.body.utilities)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('admin').findOne({ _id: ObjectID(req.body.uid) },function(err, adminInfo){
		if(adminInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})

		const multimedia = [];
		if(req.files.length > 0){
			req.files.forEach(element => {
				multimedia.push(element.path.replace(/\\/g, "/"));
			});
		}
		const tenant = [];
		if(req.body.tenant.length > 0){
			req.body.tenant.forEach(element => {
				tenant.push(ObjectID(element));
			});
		}
		const monthly_payments = [];
		if(req.body.monthly_payments && req.body.monthly_payments.length > 0){
			req.body.monthly_payments.forEach(element => {
				if(typeof element === "string"){
					element = JSON.parse(element);
				}
				element._id = ObjectID(element._id);
				monthly_payments.push(element);
			});
		}
		let insertObj = {
			propertyId: ObjectID(req.body.propertyId),
            uid: ObjectID(req.body.uid),
            simple_unit: req.body.simple_unit,
			name: req.body.name,
			size: parseInt(req.body.size),
			rent: parseInt(req.body.rent),
			deposit: parseInt(req.body.deposit),
			monthly_payments,
			tenant,
			bedrooms: parseInt(req.body.bedrooms),
			bathrooms: parseInt(req.body.bathrooms),
			balcony: req.body.balcony,
			backyard: req.body.backyard,
			lawn: req.body.lawn,
			utilities: req.body.utilities,
			multimedia,
			created_date: new Date()
		}
		db.collection('apartment').insertOne(insertObj, function(err, apartmentData) {
			if(apartmentData && apartmentData.ops && apartmentData.ops[0])
				return res.send({message: "Apartment added successfully.", status: 1, data: apartmentData.ops[0]})

			return res.send({message: "Something went wrong.", status: 0, data: {}})
		})
	})
}

async function listApartment(req, res){		// skip
	if(!req.body.uid || req.body.uid.length != 24 || !req.query.skip)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('admin').findOne({ _id: ObjectID(req.body.uid) }, function(err, adminInfo){
		if(adminInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
			
		db.collection('apartment').find({ }).skip(parseInt(req.query.skip)).limit(10).sort({created_date: -1}).toArray(function(err, apartmentList){
			res.send({message: "Apartment get successfully.", status: 1, data: {apartmentList}})
		})
	})
}

async function listMonthlyPayments(req, res){
	if(!req.body.uid || req.body.uid.length != 24)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('admin').findOne({ _id: ObjectID(req.body.uid) }, function(err, adminInfo){
		if(adminInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
			
		db.collection('monthly_payments').find({ }).toArray(function(err, monthlyPaymentsList){
			res.send({message: "Monthly payments get successfully.", status: 1, data: {monthlyPaymentsList}})
		})
	})
}

module.exports = router;
