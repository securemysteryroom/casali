const config = require('../../config.json');
const jwtAuth = require('../../common/jwt_auth');
const encryptDecrypt = require('../../common/encrypt_decrypt');
const jwt = require('jsonwebtoken');
const express = require('express');
const router = express.Router();

router.post('/login_tenant', loginTenant);
router.post('/change_password_tenant', jwtAuth.ensureToken, changePasswordTenant);
router.get('/', jwtAuth.ensureToken, tenantProfileInfo);
router.put('/', jwtAuth.ensureToken, tenantProfileEdit);

async function loginTenant(req, res){			// email, password
	if(!req.body.email || !req.body.password)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('tenant').findOne({ email: req.body.email },function(err, tenantInfo){
		if(tenantInfo == null)
			return res.send({message: "Email not registered.", status: 0, data: {}})

		let hash = encryptDecrypt.encodeDecode(req.body.password);
		if(tenantInfo.password != hash)
			return res.send({message: "You have entered wrong password.", status: 0, data: {}})
			
		const token = jwt.sign({ uid: tenantInfo._id }, config.JWT_SECRET_KEY);       					// token expire default time is 7 days
		// const token = jwt.sign({ uid: tenantInfo.uid }, config.JWT_SECRET_KEY,{ expiresIn: 20 });		// token expire in 20 seconds
		// const token = jwt.sign({ uid: tenantInfo.uid }, config.JWT_SECRET_KEY,{ expiresIn: "20" });   // token expire in 20 miliseconds, you can also set token expire time "2 days", "10h", "7d"
		tenantInfo.token = token;
		tenantInfo.uid = tenantInfo._id;
		tenantInfo.image_url = config.IMAGE_URL;
		delete tenantInfo._id
		delete tenantInfo.password;
		return res.send({message: "Tenant login successfully.", status: 1, data: tenantInfo})
	})
}

async function changePasswordTenant(req, res){		// oldPassword, newPassword
	if(!req.body.uid || req.body.uid.length != 24 || !req.body.oldPassword || !req.body.newPassword)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('tenant').findOne({ _id: ObjectID(req.body.uid) },function(err, tenantInfo){
		if(tenantInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
			
		let hash = encryptDecrypt.encodeDecode(req.body.oldPassword);
		if(tenantInfo.password != hash)
			return res.send({message: "You have entered wrong old password.", status: 0, data: {}})

		let newHash = encryptDecrypt.encodeDecode(req.body.newPassword);
		db.collection("tenant").updateOne({ _id: ObjectID(req.body.uid) }, { $set: { password: newHash }}, function(){ });
		return res.send({message: "Password update successfully.", status: 1, data: {}})
	})
}

async function tenantProfileInfo(req, res){
	if(!req.body.uid || req.body.uid.length != 24)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('tenant').findOne({ _id: ObjectID(req.body.uid) },function(err, tenantInfo){
		if(tenantInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
			
		return res.send({message: "Tenant info get successfully.", status: 1, data: tenantInfo})
	})
}

async function tenantProfileEdit(req, res){
	if(!req.body.uid || req.body.uid.length != 24)
		return res.send({message: "Invalid parameters.", status: 0, data: {}})

	db.collection('tenant').findOne({ _id: ObjectID(req.body.uid) },function(err, tenantInfo){
		if(tenantInfo == null)
			return res.send({message: "User not found.", status: 0, data: {}})
			
		if(req.body.telephone && req.body.telephone.length === 0)		// we need at least one telephone number for generate tenant password
			return res.send({message: "Please add at least one telephone number.", status: 0, data: {}})

		let updateObj = { $set: {} };
		if(req.body.name)
			updateObj.$set.name = req.body.name;
		if(req.body.trn)
			updateObj.$set.trn = parseInt(req.body.trn);
		if(req.body.telephone)
			updateObj.$set.telephone = req.body.telephone;
		if(req.body.email)
			updateObj.$set.email = req.body.email;
		if(req.body.facebook)
			updateObj.$set.facebook = req.body.facebook;
		if(req.body.whatsapp)
			updateObj.$set.whatsapp = req.body.whatsapp;
		if(Object.keys(updateObj.$set).length === 0)	// not any tenant info for update
			return res.send({message: "No data for update.", status: 0, data: {}})
		else{
			db.collection("tenant").findAndModify({ _id: ObjectID(req.body.uid) }, {}, updateObj, { upsert: true, new: true }, function(err, updatedTenantInfo){
				return res.send({message: "Tenant info update successfully.", status: 1, data: updatedTenantInfo.value})
			});
		}
	})
}

module.exports = router;
